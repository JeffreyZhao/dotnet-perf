﻿namespace Demo
{
    using System;
    using System.Diagnostics;
    using Demo.Diagnostics;

    class Program
    {
        static void Main()
        {
            var cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            var cpuCounter2 = new PerfCounter("Processor", "% Processor Time", PerfCounterInstance.Parse("_Total"));
            var buffer = new byte[1024 * 1024];

            CodeTimer.Initialize();

            // Console.WriteLine("Start collecting and press enter to continue.");
            // Console.ReadLine();

            const int iteration = 100000;

            CodeTimer.Time("Perf Counter (System)", iteration, () => cpuCounter.NextValue());
            CodeTimer.Time("Perf Counter (Faster)", iteration, () => cpuCounter2.NextValue(ref buffer));

            Console.WriteLine("Get snapshot and press enter to exit.");
            Console.ReadLine();
        }
    }
}
