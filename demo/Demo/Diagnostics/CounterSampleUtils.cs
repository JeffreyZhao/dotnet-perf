﻿namespace Demo.Diagnostics
{
  using System;
  using System.Diagnostics;
  using System.Runtime.InteropServices;
  using Demo.Win32;

  public class PerfCounterInstance
  {
    public static PerfCounterInstance Parse(string instance)
    {
      var i = instance.IndexOf('#');
      return i < 0
        ? new PerfCounterInstance(instance, 0)
        : new PerfCounterInstance(instance.Substring(0, i), Int32.Parse(instance.Substring(i + 1)));
    }

    public readonly string Name;
    public readonly int Index;

    public PerfCounterInstance(string name, int index)
    {
      Name = name;
      Index = index;
    }
  }

  internal class CounterSampleUtils
  {
    public static unsafe CounterSample ParseNextSample(byte[] data, int categoryIndex, int counterIndex, PerfCounterInstance instance = null)
    {
      var dataBlock = new NativeMethods.PERF_DATA_BLOCK();

      fixed (byte* dataPtr = data)
      {
        var dataRef = new IntPtr(dataPtr);

        Marshal.PtrToStructure(dataRef, dataBlock);

        dataRef = (IntPtr)((long)dataRef + dataBlock.HeaderLength);

        var numPerfObjects = dataBlock.NumObjectTypes;
        if (numPerfObjects == 0)
          throw new InvalidOperationException("Cannot find category: " + categoryIndex);

        //Need to find the right category, GetPerformanceData might return
        //several of them.
        var perfObject = new NativeMethods.PERF_OBJECT_TYPE();
        var foundCategory = false;

        for (var index = 0; index < numPerfObjects; index++)
        {
          Marshal.PtrToStructure(dataRef, perfObject);

          if (perfObject.ObjectNameTitleIndex == categoryIndex)
          {
            foundCategory = true;
            break;
          }

          dataRef = (IntPtr)((long)dataRef + perfObject.TotalByteLength);
        }

        if (!foundCategory)
          throw new InvalidOperationException("Cannot read category of index " + categoryIndex);

        var counterNumber = perfObject.NumCounters;
        var instanceNumber = perfObject.NumInstances;
        var isMultiInstance = instanceNumber != -1;

        // Move pointer forward to end of PERF_OBJECT_TYPE
        dataRef = (IntPtr)((long)dataRef + perfObject.HeaderLength);

        NativeMethods.PERF_COUNTER_DEFINITION perfCounterRead = null;
        NativeMethods.PERF_COUNTER_DEFINITION perfCounter = null;

        for (var index = 0; index < counterNumber; ++index)
        {
          if (perfCounterRead == null)
          {
            perfCounterRead = new NativeMethods.PERF_COUNTER_DEFINITION();
          }

          Marshal.PtrToStructure(dataRef, perfCounterRead);

          dataRef = (IntPtr)((long)dataRef + perfCounterRead.ByteLength);

          if (perfCounterRead.CounterNameTitleIndex != counterIndex)
            continue;

          Debug.Assert(perfCounter == null);

          perfCounter = perfCounterRead;
          perfCounterRead = null;
        }

        if (perfCounter == null)
          throw new InvalidOperationException("Cannot read counter of index " + counterIndex);

        var size = perfCounter.CounterSize;
        var offset = perfCounter.CounterOffset;
        long? rawValue = null;

        // now set up the InstanceNameTable.  
        if (!isMultiInstance)
        {
          rawValue = ReadValue(dataRef, size, offset);
        }
        else
        {
          Debug.Assert(instance != null);

          var currInstanceIndex = 0;
          var perfInstance = new NativeMethods.PERF_INSTANCE_DEFINITION();

          for (var i = 0; i < instanceNumber; i++)
          {
            Marshal.PtrToStructure(dataRef, perfInstance);

            var currInstanceName = Marshal.PtrToStringUni((IntPtr)((long)dataRef + perfInstance.NameOffset));

            Debug.Assert(currInstanceName != null);

            dataRef = (IntPtr)((long)dataRef + perfInstance.ByteLength);

            if (currInstanceName == instance.Name)
            {
              if (currInstanceIndex == instance.Index)
              {
                rawValue = ReadValue(dataRef, size, offset);
                break;
              }

              currInstanceIndex++;
            }

            dataRef = (IntPtr)((long)dataRef + Marshal.ReadInt32(dataRef));
          }
        }

        if (!rawValue.HasValue)
          throw new InvalidOperationException("Cannot find instance: " + instance.Name);

        return new CounterSample(rawValue.Value,
          0,
          perfObject.PerfFreq,
          dataBlock.PerfFreq,
          dataBlock.PerfTime,
          dataBlock.PerfTime100nSec,
          (PerformanceCounterType)perfCounter.CounterType,
          perfObject.PerfTime);
      }
    }

    private static long ReadValue(IntPtr pointer, int size, int offset)
    {
      if (size == 4)
      {
        return (uint)Marshal.ReadInt32((IntPtr)((long)pointer + offset));
      }
      else if (size == 8)
      {
        return Marshal.ReadInt64((IntPtr)((long)pointer + offset));
      }

      return -1;
    }
  }
}
