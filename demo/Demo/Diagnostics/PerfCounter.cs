﻿namespace Demo.Diagnostics
{
  using System;
  using System.Collections;
  using System.Diagnostics;
  using System.Globalization;
  using System.Reflection;
  using Demo.Win32;

  public class PerfCounter
  {
    // ReSharper disable once InconsistentNaming
    private const int EnglishLCID = 0x009;

    private static readonly object PerformanceCounterLib = GetPerformanceCounterLib();

    private static object GetPerformanceCounterLib()
    {
      var performanceCounterLibType = typeof(PerformanceCounter).Assembly.GetType("System.Diagnostics.PerformanceCounterLib");
      var getPerformanceCounterLibMethod = performanceCounterLibType.GetMethod("GetPerformanceCounterLib", BindingFlags.NonPublic | BindingFlags.Static);

      return getPerformanceCounterLibMethod.Invoke(null, new object[] { ".", new CultureInfo(EnglishLCID) });
    }

    private static object GetCategoryEntry(string categoryName)
    {
      var categoryTableProperty = PerformanceCounterLib.GetType().GetProperty("CategoryTable", BindingFlags.NonPublic | BindingFlags.Instance);
      var categoryTable = (Hashtable)categoryTableProperty.GetValue(PerformanceCounterLib);

      return categoryTable[categoryName];
    }

    private static int GetCategoryIndex(object counterEntry)
    {
      var counterIndexesProperty = counterEntry.GetType().GetField("NameIndex", BindingFlags.NonPublic | BindingFlags.Instance);
      Debug.Assert(counterIndexesProperty != null, "counterIndexesProperty != null");
      return (int)counterIndexesProperty.GetValue(counterEntry);
    }

    private static int GetCounterIndex(object counterEntry, string counter)
    {
      var nameTableProperty = PerformanceCounterLib.GetType().GetProperty("NameTable", BindingFlags.NonPublic | BindingFlags.Instance);
      var nameTable = (Hashtable)nameTableProperty.GetValue(PerformanceCounterLib);

      var counterIndexesProperty = counterEntry.GetType().GetField("CounterIndexes", BindingFlags.NonPublic | BindingFlags.Instance);
      Debug.Assert(counterIndexesProperty != null, "counterIndexesProperty != null");
      var counterIndexes = (int[])counterIndexesProperty.GetValue(counterEntry);

      foreach (var counterIndex in counterIndexes)
      {
        var counterName = (string)nameTable[counterIndex];

        if (String.Equals(counterName, counter, StringComparison.OrdinalIgnoreCase))
          return counterIndex;
      }

      throw new InvalidOperationException("Cannot read counter: " + counter);
    }

    private readonly int _categoryIndex;
    private readonly string _categoryIndexStr;
    private readonly int _counterIndex;
    private readonly PerfCounterInstance _instance;

    public PerfCounter(string categoryName, string counterName, PerfCounterInstance instance)
    {
      var entry = GetCategoryEntry(categoryName);

      _categoryIndex = GetCategoryIndex(entry);
      _categoryIndexStr = _categoryIndex.ToString(CultureInfo.InvariantCulture);
      _counterIndex = GetCounterIndex(entry, counterName);
      _instance = instance;
    }

    private CounterSample _oldSample;

    private CounterSample NextSample(ref byte[] buffer)
    {
      RegistryKeyHelper.GetPerformanceData(_categoryIndexStr, ref buffer);

      return CounterSampleUtils.ParseNextSample(buffer, _categoryIndex, _counterIndex, _instance);
    }

    public float NextValue(ref byte[] buffer)
    {
      var newSample = NextSample(ref buffer);

      var retVal = CounterSample.Calculate(_oldSample, newSample);
      _oldSample = newSample;

      return retVal;
    }
  }
}
