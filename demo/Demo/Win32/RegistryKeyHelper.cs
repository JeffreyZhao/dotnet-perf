﻿namespace Demo.Win32
{
  using System;
  using System.Diagnostics;
  using System.Reflection;
  using Microsoft.Win32;
  using Microsoft.Win32.SafeHandles;

  public static class RegistryKeyHelper
  {
    public static void GetPerformanceData(string nameIndex, ref byte[] buffer)
    {
      InternalGetPerformanceData(nameIndex, ref buffer);
    }

    private static readonly FieldInfo HKeyField = typeof(RegistryKey).GetField("hkey", BindingFlags.NonPublic | BindingFlags.Instance);
    private static readonly MethodInfo Win32ErrorMethod = typeof(RegistryKey).GetMethod("Win32Error", BindingFlags.NonPublic | BindingFlags.Instance);
    private static readonly SafeRegistryHandle PerDataRegistryHandle = GetHandle(Registry.PerformanceData);

    private static SafeRegistryHandle GetHandle(RegistryKey registryKey)
    {
      return (SafeRegistryHandle)HKeyField.GetValue(registryKey);
    }

    private static void Win32Error(this RegistryKey registryKey, int ret, string str)
    {
      Win32ErrorMethod.Invoke(registryKey, new object[] { ret, str });
    }

    private static void InternalGetPerformanceData(string nameIndex, ref byte[] buffer)
    {
      Debug.Assert(PerDataRegistryHandle != null, "The registry key has been disposed.");

      if (buffer == null)
      {
        buffer = new byte[512 * 1024];
      }

      var size = buffer.Length;

      var type = 0;
      var sizeInput = size;

      int r;

      while (Win32Native.ERROR_MORE_DATA == (r = Win32Native.RegQueryValueEx(PerDataRegistryHandle, nameIndex, null, ref type, buffer, ref sizeInput)))
      {
        if (size == Int32.MaxValue)
        {
          // ERROR_MORE_DATA was returned however we cannot increase the buffer size beyond Int32.MaxValue
          Registry.PerformanceData.Win32Error(r, nameIndex);
        }
        else if (size > (Int32.MaxValue / 2))
        {
          // at this point in the loop "size * 2" would cause an overflow
          size = Int32.MaxValue;
        }
        else
        {
          size *= 2;
        }

        sizeInput = size;
        buffer = new byte[size];
      }

      if (r != 0)
        Registry.PerformanceData.Win32Error(r, nameIndex);
    }
  }
}
