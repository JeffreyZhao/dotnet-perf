﻿namespace Demo.Win32
{
  using System;
  using System.Runtime.InteropServices;
  using System.Runtime.Versioning;
  using Microsoft.Win32.SafeHandles;

  public static class Win32Native
  {
    internal const int REG_OPTION_NON_VOLATILE = 0x0000;     // (default) keys are persisted beyond reboot/unload
    internal const int REG_OPTION_VOLATILE = 0x0001;     // All keys created by the function are volatile
    internal const int REG_OPTION_CREATE_LINK = 0x0002;     // They key is a symbolic link
    internal const int REG_OPTION_BACKUP_RESTORE = 0x0004;  // Use SE_BACKUP_NAME process special privileges
    internal const int REG_NONE = 0;     // No value type
    internal const int REG_SZ = 1;     // Unicode nul terminated string
    internal const int REG_EXPAND_SZ = 2;     // Unicode nul terminated string
    // (with environment variable references)
    internal const int REG_BINARY = 3;     // Free form binary
    internal const int REG_DWORD = 4;     // 32-bit number
    internal const int REG_DWORD_LITTLE_ENDIAN = 4;     // 32-bit number (same as REG_DWORD)
    internal const int REG_DWORD_BIG_ENDIAN = 5;     // 32-bit number
    internal const int REG_LINK = 6;     // Symbolic Link (unicode)
    internal const int REG_MULTI_SZ = 7;     // Multiple Unicode strings
    internal const int REG_RESOURCE_LIST = 8;     // Resource list in the resource map
    internal const int REG_FULL_RESOURCE_DESCRIPTOR = 9;   // Resource list in the hardware description
    internal const int REG_RESOURCE_REQUIREMENTS_LIST = 10;
    internal const int REG_QWORD = 11;    // 64-bit number

    internal const int ERROR_SUCCESS = 0x0;
    internal const int ERROR_INVALID_FUNCTION = 0x1;
    internal const int ERROR_FILE_NOT_FOUND = 0x2;
    internal const int ERROR_PATH_NOT_FOUND = 0x3;
    internal const int ERROR_ACCESS_DENIED = 0x5;
    internal const int ERROR_INVALID_HANDLE = 0x6;
    internal const int ERROR_NOT_ENOUGH_MEMORY = 0x8;
    internal const int ERROR_INVALID_DATA = 0xd;
    internal const int ERROR_INVALID_DRIVE = 0xf;
    internal const int ERROR_NO_MORE_FILES = 0x12;
    internal const int ERROR_NOT_READY = 0x15;
    internal const int ERROR_BAD_LENGTH = 0x18;
    internal const int ERROR_SHARING_VIOLATION = 0x20;
    internal const int ERROR_NOT_SUPPORTED = 0x32;
    internal const int ERROR_FILE_EXISTS = 0x50;
    internal const int ERROR_INVALID_PARAMETER = 0x57;
    internal const int ERROR_BROKEN_PIPE = 0x6D;
    internal const int ERROR_CALL_NOT_IMPLEMENTED = 0x78;
    internal const int ERROR_INSUFFICIENT_BUFFER = 0x7A;
    internal const int ERROR_INVALID_NAME = 0x7B;
    internal const int ERROR_BAD_PATHNAME = 0xA1;
    internal const int ERROR_ALREADY_EXISTS = 0xB7;
    internal const int ERROR_ENVVAR_NOT_FOUND = 0xCB;
    internal const int ERROR_FILENAME_EXCED_RANGE = 0xCE;  // filename too long.
    internal const int ERROR_NO_DATA = 0xE8;
    internal const int ERROR_PIPE_NOT_CONNECTED = 0xE9;
    internal const int ERROR_MORE_DATA = 0xEA;
    internal const int ERROR_DIRECTORY = 0x10B;
    internal const int ERROR_OPERATION_ABORTED = 0x3E3;  // 995; For IO Cancellation
    internal const int ERROR_NOT_FOUND = 0x490;          // 1168; For IO Cancellation
    internal const int ERROR_NO_TOKEN = 0x3f0;
    internal const int ERROR_DLL_INIT_FAILED = 0x45A;
    internal const int ERROR_NON_ACCOUNT_SID = 0x4E9;
    internal const int ERROR_NOT_ALL_ASSIGNED = 0x514;
    internal const int ERROR_UNKNOWN_REVISION = 0x519;
    internal const int ERROR_INVALID_OWNER = 0x51B;
    internal const int ERROR_INVALID_PRIMARY_GROUP = 0x51C;
    internal const int ERROR_NO_SUCH_PRIVILEGE = 0x521;
    internal const int ERROR_PRIVILEGE_NOT_HELD = 0x522;
    internal const int ERROR_NONE_MAPPED = 0x534;
    internal const int ERROR_INVALID_ACL = 0x538;
    internal const int ERROR_INVALID_SID = 0x539;
    internal const int ERROR_INVALID_SECURITY_DESCR = 0x53A;
    internal const int ERROR_BAD_IMPERSONATION_LEVEL = 0x542;
    internal const int ERROR_CANT_OPEN_ANONYMOUS = 0x543;
    internal const int ERROR_NO_SECURITY_ON_OBJECT = 0x546;
    internal const int ERROR_TRUSTED_RELATIONSHIP_FAILURE = 0x6FD;

    [DllImport("advapi32.dll", CharSet = CharSet.Auto, BestFitMapping = false)]
    [ResourceExposure(ResourceScope.None)]
    internal static extern int RegQueryValueEx(
      SafeRegistryHandle hKey,
      String lpValueName,
      int[] lpReserved,
      ref int lpType,
      [Out] byte[] lpData,
      ref int lpcbData);
  }
}
